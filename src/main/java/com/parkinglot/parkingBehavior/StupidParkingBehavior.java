package com.parkinglot.parkingBehavior;

import com.parkinglot.Car;
import com.parkinglot.ParkingLot;
import com.parkinglot.ParkingTicket;

import java.util.List;

public class StupidParkingBehavior implements ParkingBehavior {
    @Override
    public ParkingTicket park(List<ParkingLot> parkingLots, Car car) {
        ParkingLot findParkingLot = parkingLots.stream().filter(parkingLot -> !parkingLot.hasNotPosition()).findFirst().orElse(parkingLots.get(0));
        return findParkingLot.park(car);
    }
}