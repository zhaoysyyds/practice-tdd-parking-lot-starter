package com.parkinglot.parkingBehavior;

import com.parkinglot.Car;
import com.parkinglot.ParkingLot;
import com.parkinglot.ParkingTicket;

import java.util.List;

public class SmartParkingBehavior implements ParkingBehavior {
    @Override
    public ParkingTicket park(List<ParkingLot> parkingLots, Car car) {
        ParkingLot smartFindParkingLot = parkingLots.stream()
                .reduce((preParkingLot, nowParkingLot) -> preParkingLot.vancantSpaceLargerThan(nowParkingLot) ? preParkingLot : nowParkingLot).get();
        return smartFindParkingLot.park(car);
    }
}