package com.parkinglot.parkingBehavior;

import com.parkinglot.Car;
import com.parkinglot.ParkingLot;
import com.parkinglot.ParkingTicket;

import java.util.List;

public interface ParkingBehavior {
    public ParkingTicket park(List<ParkingLot> parkingLots, Car car);
}
