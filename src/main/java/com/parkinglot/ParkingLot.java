package com.parkinglot;

import java.util.HashMap;

public class ParkingLot {
    protected int capacity;
    protected int id;
    protected HashMap<ParkingTicket, Car> parkingTicketMap;

    public ParkingLot(int capacity) {
        this.capacity = capacity;
        parkingTicketMap = new HashMap<>();
    }

    public ParkingLot(int capacity, int id) {
        this.capacity = capacity;
        this.id = id;
        parkingTicketMap = new HashMap<>();
    }

    public ParkingTicket park(Car car) throws UnrecognizedParkingTicketException {
        if (hasNotPosition()) {
            throw new NoAvailablePositionException();
        }
        ParkingTicket parkingTicket = new ParkingTicket(this.id);
        this.parkingTicketMap.put(parkingTicket, car);
        return parkingTicket;
    }

    public Car fetch(ParkingTicket parkingTicket) throws UnrecognizedParkingTicketException {
        if (!hasTicket(parkingTicket)) {
            throw new UnrecognizedParkingTicketException();
        }
        return parkingTicketMap.remove(parkingTicket);
    }

    public boolean hasNotPosition() {
        return parkingTicketMap.size() == capacity;
    }

    public boolean hasTicket(ParkingTicket parkingTicket) {
        return parkingTicketMap.containsKey(parkingTicket);
    }

    public int getVancantSpace() {
        return this.capacity - this.parkingTicketMap.size();
    }

    public boolean vancantSpaceLargerThan(ParkingLot parkingLot) {
        return this.getVancantSpace() >= parkingLot.getVancantSpace();
    }

    public boolean vancantSpaceRatioLargerThan(ParkingLot parkingLot) {
        return (this.getVancantSpace() * 1.0 / this.capacity) >= (parkingLot.getVancantSpace() * 1.0 / parkingLot.capacity);
    }
}
