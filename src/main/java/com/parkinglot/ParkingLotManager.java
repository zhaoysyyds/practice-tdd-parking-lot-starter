package com.parkinglot;

import com.parkinglot.parkingBehavior.StupidParkingBehavior;

import java.util.ArrayList;
import java.util.List;

public class ParkingLotManager extends ParkingBoy {

    protected List<ParkingBoy> parkingBoys = new ArrayList<>();

    public ParkingLotManager(List<ParkingLot> parkingLots) {
        super(parkingLots, new StupidParkingBehavior());
    }

    public void addParkingBoy(List<ParkingBoy> parkingBoyList) {
        parkingBoyList.forEach(parkingBoy -> parkingBoys.add(parkingBoy));
    }

    public ParkingTicket specifyBoyPark(int parkingBoyIndex, Car car) {
        return parkingBoys.get(parkingBoyIndex).park(car);
    }

    public Car specifyBoyFetch(int parkingBoyIndex, ParkingTicket parkingTicket) {
        return parkingBoys.get(parkingBoyIndex).fetch(parkingTicket);
    }
}
