package com.parkinglot;

import com.parkinglot.parkingBehavior.ParkingBehavior;

import java.util.List;

public class ParkingBoy {
    protected List<ParkingLot> parkingLots;

    public ParkingBehavior parkingBehavior;
    public ParkingBoy(List<ParkingLot> parkingLots, ParkingBehavior parkingBehavior) {
        this.parkingLots = parkingLots;
        this.parkingBehavior = parkingBehavior;
    }

    public Car fetch(ParkingTicket ticket) {
        ParkingLot findParkingLot = parkingLots.stream().filter(parkingLot -> parkingLot.hasTicket(ticket)).findFirst().orElse(parkingLots.get(0));
        return findParkingLot.fetch(ticket);
    }

    public ParkingTicket park(Car car) {
        return parkingBehavior.park(parkingLots, car);
    }
}
