package com.parkinglot;

import com.parkinglot.parkingBehavior.StupidParkingBehavior;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class ParkingBoyTest {
    @Test
    void should_return_ticket_when_park_given_parking_lot_and_parking_boy_and_car() {
        //given
        ParkingLot parkingLot = new ParkingLot(10);
        ParkingBoy parkingBoy = new ParkingBoy(List.of(parkingLot), new StupidParkingBehavior());
        Car car = new Car();
        //when
        ParkingTicket parkingTicket = parkingBoy.park(car);
        //then
        assertNotNull(parkingTicket);
    }

    @Test
    void should_return_car_when_fetch_given_parking_lot_and_parking_boy_and_ticket() {
        //given
        ParkingLot parkingLot = new ParkingLot(10);
        ParkingBoy parkingBoy = new ParkingBoy(List.of(parkingLot),new StupidParkingBehavior());
        Car car = new Car();
        ParkingTicket parkingTicket = parkingBoy.park(car);
        //when
        Car returnCar = parkingLot.fetch(parkingTicket);
        //then
        assertNotNull(returnCar);
    }

    @Test
    void should_return_right_car_with_each_ticket_when_fetch_given_parking_lot_with_two_parked_cars_and_parking_boy_and_two_tickets() {
        //given
        ParkingLot parkingLot = new ParkingLot(10);
        ParkingBoy parkingBoy = new ParkingBoy(List.of(parkingLot),new StupidParkingBehavior());
        Car car1 = new Car();
        Car car2 = new Car();
        ParkingTicket parkingTicket1 = parkingBoy.park(car1);
        ParkingTicket parkingTicket2 = parkingBoy.park(car2);

        //when
        Car returnCar1 = parkingBoy.fetch(parkingTicket1);
        Car returnCar2 = parkingBoy.fetch(parkingTicket2);
        //then
        assertEquals(car1, returnCar1);
        assertEquals(car2, returnCar2);
        assertNotEquals(returnCar1, returnCar2);
    }

    @Test
    void should_return_nothing_when_fetch_given_parking_lot_and_parking_boy_and_wrong_ticket() {
        //given
        ParkingLot parkingLot = new ParkingLot(10);
        ParkingBoy parkingBoy = new ParkingBoy(List.of(parkingLot),new StupidParkingBehavior());
        Car car = new Car();
        parkingBoy.park(car);
        ParkingTicket wrongParkingTicket = new ParkingTicket(22);

        //when
        //then
        UnrecognizedParkingTicketException exception = assertThrows(UnrecognizedParkingTicketException.class, () -> parkingBoy.fetch(wrongParkingTicket));
        assertEquals("Unrecognized parking ticket.", exception.getMessage());

    }

    @Test
    void should_return_nothing_when_fetch_given_parking_lot_and_parking_boy_and_used_ticket() {
        //given
        ParkingLot parkingLot = new ParkingLot(10);
        ParkingBoy parkingBoy = new ParkingBoy(List.of(parkingLot),new StupidParkingBehavior());
        Car car = new Car();
        ParkingTicket parkingTicket = parkingBoy.park(car);
        parkingBoy.fetch(parkingTicket);

        //when
        //then
        UnrecognizedParkingTicketException exception = assertThrows(UnrecognizedParkingTicketException.class, () -> parkingBoy.fetch(parkingTicket));
        assertEquals("Unrecognized parking ticket.", exception.getMessage());
    }

    @Test
    void should_return_nothing_when_park_given_parking_lot_without_any_position_and_parking_boy_and_car() {
        //given
        ParkingLot parkingLot = new ParkingLot(10);
        ParkingBoy parkingBoy = new ParkingBoy(List.of(parkingLot),new StupidParkingBehavior());
        for (int i = 0; i < 10; i++) {
            parkingBoy.park(new Car());
        }
        Car car = new Car();

        //when
        //then
        NoAvailablePositionException exception = assertThrows(NoAvailablePositionException.class, () -> parkingBoy.park(car));
        assertEquals("No available position.", exception.getMessage());
    }


}
