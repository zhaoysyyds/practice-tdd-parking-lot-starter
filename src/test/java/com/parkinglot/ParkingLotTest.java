package com.parkinglot;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class ParkingLotTest {
    @Test
    void should_return_ticket_when_park_given_parking_boy_lot_and_car() {
        //given
        ParkingLot parkingLot = new ParkingLot(10);
        Car car = new Car();
        //when
        ParkingTicket parkingTicket = parkingLot.park(car);
        //then
        assertNotNull(parkingTicket);
    }

    @Test
    void should_return_car_when_fetch_given_parking_lot_and_ticket() {
        //given
        ParkingLot parkingLot = new ParkingLot(10);
        Car car = new Car();
        ParkingTicket parkingTicket = parkingLot.park(car);
        //when
        Car returnCar = parkingLot.fetch(parkingTicket);
        System.out.println(car);
        //then
        assertNotNull(returnCar);
    }

    @Test
    void should_return_right_car_with_each_ticket_when_fetch_given_parking_lot_with_two_parked_cars_and_two_tickets() {
        //given
        ParkingLot parkingLot = new ParkingLot(10);
        Car car1 = new Car();
        Car car2 = new Car();
        ParkingTicket parkingTicket1 = parkingLot.park(car1);
        ParkingTicket parkingTicket2 = parkingLot.park(car2);

        //when
        Car returnCar1 = parkingLot.fetch(parkingTicket1);
        Car returnCar2 = parkingLot.fetch(parkingTicket2);
        //then
        assertEquals(car1, returnCar1);
        assertEquals(car2, returnCar2);
        assertNotEquals(returnCar1, returnCar2);
    }

    @Test
    void should_return_nothing_when_fetch_given_parking_lot_and_wrong_ticket() {
        //given
        ParkingLot parkingLot = new ParkingLot(10);
        Car car = new Car();
        parkingLot.park(car);
        ParkingTicket wrongParkingTicket = new ParkingTicket(22);

        //when
        //then
        UnrecognizedParkingTicketException exception = assertThrows(UnrecognizedParkingTicketException.class, () -> parkingLot.fetch(wrongParkingTicket));
        assertEquals("Unrecognized parking ticket.", exception.getMessage());

    }

    @Test
    void should_return_nothing_when_fetch_given_parking_lot_and_used_ticket() {
        //given
        ParkingLot parkingLot = new ParkingLot(10);
        Car car = new Car();
        ParkingTicket parkingTicket = parkingLot.park(car);
        parkingLot.fetch(parkingTicket);

        //when
        //then
        UnrecognizedParkingTicketException exception = assertThrows(UnrecognizedParkingTicketException.class, () -> parkingLot.fetch(parkingTicket));
        assertEquals("Unrecognized parking ticket.", exception.getMessage());
    }

    @Test
    void should_return_nothing_when_park_given_parking_lot_without_any_position_and_car() {
        //given
        ParkingLot parkingLot = new ParkingLot(10);
        for (int i = 0; i < 10; i++) {
            parkingLot.park(new Car());
        }
        Car car = new Car();

        //when
        //then
        NoAvailablePositionException exception = assertThrows(NoAvailablePositionException.class, () -> parkingLot.park(car));
        assertEquals("No available position.", exception.getMessage());
    }


}
