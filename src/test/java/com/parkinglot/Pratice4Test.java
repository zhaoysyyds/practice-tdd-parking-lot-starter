package com.parkinglot;

import com.parkinglot.parkingBehavior.StupidParkingBehavior;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class Pratice4Test {
    @Test
    void should_car_park_to_the_first_parking_lot_ticket_when_park_given_two_parking_lots_both_with_available_position_and_parking_boy_and_car() {
        //given
        ParkingLot parkingLot1 = new ParkingLot(10);
        ParkingLot parkingLot2 = new ParkingLot(10);
        ParkingBoy parkingBoy = new ParkingBoy(List.of(parkingLot1,parkingLot2),new StupidParkingBehavior());
        Car car = new Car();
        //when
        ParkingTicket parkingTicket = parkingBoy.park(car);
        //then
        assertNotNull(parkingTicket);
    }

    @Test
    void should_car_park_to_the_second_parking_lot_ticket_when_park_given_two_parking_lots_first_is_full_and_second_is_available_position_and_parking_boy_and_car() {
        //given
        ParkingLot parkingLot1 = new ParkingLot(1, 1);
        ParkingLot parkingLot2 = new ParkingLot(10, 2);
        ParkingBoy parkingBoy = new ParkingBoy(List.of(parkingLot1,parkingLot2),new StupidParkingBehavior());
        parkingBoy.park(new Car());

        Car car = new Car();
        //when
        ParkingTicket parkingTicket = parkingBoy.park(car);
        //then
        assertEquals(parkingTicket.group, parkingLot2.id);
    }

    @Test
    void should_return_the_right_car_with_each_ticket_when_fetch_the_car_twice_given_a_standard_parking_boy_who_manage_two_parking_lots_both_with_a_parked_car() {
        //given
        ParkingLot parkingLot1 = new ParkingLot(1, 1);
        ParkingLot parkingLot2 = new ParkingLot(10, 2);
        ParkingBoy parkingBoy = new ParkingBoy(List.of(parkingLot1,parkingLot2),new StupidParkingBehavior());
        Car car1 = new Car();
        Car car2 = new Car();
        ParkingTicket parkingTicket1 = parkingBoy.park(car1);
        ParkingTicket parkingTicket2 = parkingBoy.park(car2);

        //when
        Car returnCar1 = parkingBoy.fetch(parkingTicket1);
        Car returnCar2 = parkingBoy.fetch(parkingTicket2);
        //then
        assertEquals(car1, returnCar1);
        assertEquals(car2, returnCar2);
    }

    @Test
    void should_return_nothing_with_error_message_Unrecognized_parking_ticket_when_fetch_given_a_standard_parking_boy_who_manage_two_parking_lots_and_an_unrecognized_ticket() {
        //given
        ParkingLot parkingLot1 = new ParkingLot(1, 1);
        ParkingLot parkingLot2 = new ParkingLot(10, 2);
        ParkingBoy parkingBoy = new ParkingBoy(List.of(parkingLot1,parkingLot2),new StupidParkingBehavior());
        ParkingTicket wrongParkingTicket = new ParkingTicket(-1);
        //when
        //then
        UnrecognizedParkingTicketException exception = assertThrows(UnrecognizedParkingTicketException.class, () -> parkingBoy.fetch(wrongParkingTicket));
        assertEquals("Unrecognized parking ticket.", exception.getMessage());
    }

    @Test
    void should_return_nothing_with_error_message_Unrecognized_parking_ticket_when_fetch_given_a_standard_parking_boy_who_manage_two_parking_lots_and_an_used_ticket() {
        //given
        ParkingLot parkingLot1 = new ParkingLot(10, 1);
        ParkingLot parkingLot2 = new ParkingLot(10, 2);
        ParkingBoy parkingBoy = new ParkingBoy(List.of(parkingLot1,parkingLot2),new StupidParkingBehavior());
        ParkingTicket usedParkingTicket = parkingBoy.park(new Car());
        parkingBoy.fetch(usedParkingTicket);
        //when
        //then
        UnrecognizedParkingTicketException exception = assertThrows(UnrecognizedParkingTicketException.class, () -> parkingBoy.fetch(usedParkingTicket));
        assertEquals("Unrecognized parking ticket.", exception.getMessage());
    }


    @Test
    void should_return_nothing_with_error_message_No_available_position_when_park_given_a_standard_parking_boy_who_manage_two_parking_lots_and_both_without_any_position_and_a_car() {
        //given
        ParkingLot parkingLot1 = new ParkingLot(1, 1);
        ParkingLot parkingLot2 = new ParkingLot(1, 2);
        ParkingBoy parkingBoy = new ParkingBoy(List.of(parkingLot1,parkingLot2),new StupidParkingBehavior());
        parkingBoy.park(new Car());
        parkingBoy.park(new Car());
        //when
        //then
        NoAvailablePositionException exception = assertThrows(NoAvailablePositionException.class, () -> parkingBoy.park(new Car()));
        assertEquals("No available position.", exception.getMessage());
    }
}
