package com.parkinglot;

import com.parkinglot.parkingBehavior.SmartParkingBehavior;
import com.parkinglot.parkingBehavior.StupidParkingBehavior;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class ParkingLotManagerTest {
    @Test
    void should_return_void_when_add_two_parking_boys_given_a_parking_log_manager_with_parking_lot_and_two_parking_boy() {
        //given
        ParkingLot parkingLot = new ParkingLot(10);
        ParkingLotManager parkingLotManager = new ParkingLotManager(List.of(parkingLot));
        ParkingBoy parkingBoy = new ParkingBoy(List.of(parkingLot), new StupidParkingBehavior());
        ParkingBoy smartParkingBoy = new ParkingBoy((List.of(parkingLot)), new SmartParkingBehavior());
        //when
        parkingLotManager.addParkingBoy(List.of(parkingBoy, smartParkingBoy));
        //then
        assertEquals(parkingBoy, parkingLotManager.parkingBoys.get(0));
        assertEquals(smartParkingBoy, parkingLotManager.parkingBoys.get(1));
    }

    @Test
    void should_return_ticket_when_park_given_a_parking_log_manager_with_parking_lot_and_two_parking_boy_and_car() {
        //given
        ParkingLot parkingLot = new ParkingLot(10);
        ParkingLotManager parkingLotManager = new ParkingLotManager(List.of(parkingLot));
        ParkingBoy parkingBoy = new ParkingBoy(List.of(parkingLot), new StupidParkingBehavior());
        ParkingBoy smartParkingBoy = new ParkingBoy((List.of(parkingLot)), new SmartParkingBehavior());
        parkingLotManager.addParkingBoy(List.of(parkingBoy, smartParkingBoy));
        Car car = new Car();
        //when
        ParkingTicket parkingTicket = parkingLotManager.park(car);
        //then
        assertNotNull(parkingTicket);
    }

    @Test
    void should_return_car_when_fetch_given_a_parking_log_manager_with_parking_lot_and_two_parking_boy_and_ticket() {
        //given
        ParkingLot parkingLot = new ParkingLot(10);
        ParkingLotManager parkingLotManager = new ParkingLotManager(List.of(parkingLot));
        ParkingBoy parkingBoy = new ParkingBoy(List.of(parkingLot), new StupidParkingBehavior());
        ParkingBoy smartParkingBoy = new ParkingBoy((List.of(parkingLot)), new SmartParkingBehavior());
        parkingLotManager.addParkingBoy(List.of(parkingBoy, smartParkingBoy));
        Car car = new Car();
        ParkingTicket parkingTicket = parkingLotManager.park(car);
        //when
        Car returnCar=parkingLotManager.fetch(parkingTicket);
        //then
        assertNotNull(returnCar);
    }

    @Test
    void should_return_ticket_when_specify_boy_park_given_a_parking_log_manager_with_parking_lot_and_an_parking_boy_and_car() {
        //given
        ParkingLot parkingLot = new ParkingLot(10);
        ParkingLotManager parkingLotManager = new ParkingLotManager(List.of(parkingLot));
        ParkingBoy parkingBoy = new ParkingBoy(List.of(parkingLot), new StupidParkingBehavior());
        parkingLotManager.addParkingBoy(List.of(parkingBoy));
        Car car = new Car();
        //when
        ParkingTicket parkingTicket = parkingLotManager.specifyBoyPark(0,car);
        //then
        assertNotNull(parkingTicket);
    }

    @Test
    void should_return_car_when_specify_boy_fetch_given_a_parking_log_manager_with_parking_lot_and_an_parking_boy_and_ticket() {
        //given
        ParkingLot parkingLot = new ParkingLot(10);
        ParkingLotManager parkingLotManager = new ParkingLotManager(List.of(parkingLot));
        ParkingBoy parkingBoy = new ParkingBoy(List.of(parkingLot), new StupidParkingBehavior());
        parkingLotManager.addParkingBoy(List.of(parkingBoy));
        Car car = new Car();
        ParkingTicket parkingTicket = parkingLotManager.specifyBoyPark(0,car);
        //when
        Car returnCar=parkingLotManager.specifyBoyFetch(0,parkingTicket);
        //then
        assertEquals(car,returnCar);
    }


    @Test
    void should_return_nothing_when_specify_boy_fetch_given_a_parking_log_manager_with_parking_lot_and_an_parking_boy_and_wrong_ticket() {
        //given
        ParkingLot parkingLot = new ParkingLot(10);
        ParkingLotManager parkingLotManager = new ParkingLotManager(List.of(parkingLot));
        ParkingBoy parkingBoy = new ParkingBoy(List.of(parkingLot), new StupidParkingBehavior());
        parkingLotManager.addParkingBoy(List.of(parkingBoy));
        Car car = new Car();
        parkingLotManager.specifyBoyPark(0,car);
        ParkingTicket wrongParkingTicket = new ParkingTicket(22);
        //when
        //then
        UnrecognizedParkingTicketException exception = assertThrows(UnrecognizedParkingTicketException.class, () -> parkingLotManager.specifyBoyFetch(0,wrongParkingTicket));
        assertEquals("Unrecognized parking ticket.", exception.getMessage());
    }
    @Test
    void should_return_nothing_when_specify_boy_fetch_given_a_parking_log_manager_with_parking_lot_and_an_parking_boy_and_used_ticket() {
        //given
        ParkingLot parkingLot = new ParkingLot(10);
        ParkingLotManager parkingLotManager = new ParkingLotManager(List.of(parkingLot));
        ParkingBoy parkingBoy = new ParkingBoy(List.of(parkingLot), new StupidParkingBehavior());
        parkingLotManager.addParkingBoy(List.of(parkingBoy));
        Car car = new Car();
        ParkingTicket usedParkingTicket=parkingLotManager.specifyBoyPark(0,car);
        parkingLotManager.specifyBoyFetch(0,usedParkingTicket);
        //when
        //then
        UnrecognizedParkingTicketException exception = assertThrows(UnrecognizedParkingTicketException.class, () -> parkingLotManager.specifyBoyFetch(0,usedParkingTicket));
        assertEquals("Unrecognized parking ticket.", exception.getMessage());
    }

    @Test
    void should_return_nothing_when_specify_boy_park_given_a_parking_log_manager_with_parking_lot_without_any_position_and_a_parking_boy() {
        //given
        ParkingLot parkingLot = new ParkingLot(1);
        ParkingLotManager parkingLotManager = new ParkingLotManager(List.of(parkingLot));
        ParkingBoy parkingBoy = new ParkingBoy(List.of(parkingLot), new StupidParkingBehavior());
        parkingLotManager.addParkingBoy(List.of(parkingBoy));
        parkingLotManager.specifyBoyPark(0,new Car());
        //when
        //then
        NoAvailablePositionException exception = assertThrows(NoAvailablePositionException.class, () -> parkingLotManager.specifyBoyPark(0,new Car()));
        assertEquals("No available position.", exception.getMessage());
    }
    @Test
    void should_return_nothing_when_specify_boy_park_given_a_parking_log_manager_with_two_parking_lots_and_one_with_position_and_other_without_position_and_a_parking_boy_with_a_parking_lot_without_position() {
        //given
        ParkingLot parkingLot1 = new ParkingLot(1);
        ParkingLot parkingLot2 = new ParkingLot(10);
        ParkingLotManager parkingLotManager = new ParkingLotManager(List.of(parkingLot1,parkingLot2));
        ParkingBoy parkingBoy = new ParkingBoy(List.of(parkingLot1), new StupidParkingBehavior());
        parkingLotManager.addParkingBoy(List.of(parkingBoy));
        parkingLotManager.park(new Car());
        //when
        //then
        NoAvailablePositionException exception = assertThrows(NoAvailablePositionException.class, () -> parkingLotManager.specifyBoyPark(0,new Car()));
        assertEquals("No available position.", exception.getMessage());
    }
}
