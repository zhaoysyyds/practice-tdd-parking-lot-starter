package com.parkinglot;

import com.parkinglot.parkingBehavior.SuperSmartParkingBehavior;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class SuperSmartParkingTest {
    @Test
    void should_car_park_to_the_first_parking_lot_ticket_when_park_given_two_parking_lots_both_with_available_position_and_lot1_vacant_capacity_ratio_greater_than_lot2_vacant_capacity_ratio_and_super_smart_parking_boy_and_car() {
        //given
        ParkingLot parkingLot1 = new ParkingLot(10, 1);
        ParkingLot parkingLot2 = new ParkingLot(9, 2);
        ParkingBoy superSmartParkingBoy = new ParkingBoy(List.of(parkingLot1, parkingLot2),new SuperSmartParkingBehavior());

        superSmartParkingBoy.park(new Car());
        superSmartParkingBoy.park(new Car());
        Car car = new Car();
        //when
        ParkingTicket parkingTicket = superSmartParkingBoy.park(car);
        //then
        assertEquals(parkingTicket.group, parkingLot1.id);
    }

    @Test
    void should_car_park_to_the_first_parking_lot_ticket_when_park_given_two_parking_lots_both_with_available_position_and_lot2_vacant_capacity_ratio_greater_than_lot1_vacant_capacity_ratio_and_super_smart_parking_boy_and_car() {
        //given
        ParkingLot parkingLot1 = new ParkingLot(10, 1);
        ParkingLot parkingLot2 = new ParkingLot(11, 2);
        ParkingBoy superSmartParkingBoy = new ParkingBoy(List.of(parkingLot1, parkingLot2),new SuperSmartParkingBehavior());
        superSmartParkingBoy.park(new Car());
        superSmartParkingBoy.park(new Car());
        Car car = new Car();
        //when
        ParkingTicket parkingTicket = superSmartParkingBoy.park(car);
        //then
        assertEquals(parkingTicket.group, parkingLot2.id);
    }

    @Test
    void should_return_the_right_car_with_each_ticket_when_fetch_the_car_twice_given_super_smart_parking_boy_who_manage_two_parking_lots_both_with_a_parked_car() {
        //given
        ParkingLot parkingLot1 = new ParkingLot(10, 1);
        ParkingLot parkingLot2 = new ParkingLot(10, 2);
        ParkingBoy superSmartParkingBoy = new ParkingBoy(List.of(parkingLot1, parkingLot2),new SuperSmartParkingBehavior());
        Car car1 = new Car();
        Car car2 = new Car();
        ParkingTicket parkingTicket1 = superSmartParkingBoy.park(car1);
        ParkingTicket parkingTicket2 = superSmartParkingBoy.park(car2);

        //when
        Car returnCar1 = superSmartParkingBoy.fetch(parkingTicket1);
        Car returnCar2 = superSmartParkingBoy.fetch(parkingTicket2);
        //then
        assertEquals(car1, returnCar1);
        assertEquals(car2, returnCar2);
    }

    @Test
    void should_return_nothing_with_error_message_Unrecognized_parking_ticket_when_fetch_given_super_smart_parking_boy_who_manage_two_parking_lots_and_an_unrecognized_ticket() {
        //given
        ParkingLot parkingLot1 = new ParkingLot(1, 1);
        ParkingLot parkingLot2 = new ParkingLot(10, 2);
        ParkingBoy superSmartParkingBoy = new ParkingBoy(List.of(parkingLot1, parkingLot2),new SuperSmartParkingBehavior());
        ParkingTicket wrongParkingTicket = new ParkingTicket(-1);
        //when
        //then
        UnrecognizedParkingTicketException exception = assertThrows(UnrecognizedParkingTicketException.class, () -> superSmartParkingBoy.fetch(wrongParkingTicket));
        assertEquals("Unrecognized parking ticket.", exception.getMessage());
    }

    @Test
    void should_return_nothing_with_error_message_Unrecognized_parking_ticket_when_fetch_given_super_smart_parking_boy_who_manage_two_parking_lots_and_an_used_ticket() {
        //given
        ParkingLot parkingLot1 = new ParkingLot(10, 1);
        ParkingLot parkingLot2 = new ParkingLot(10, 2);
        ParkingBoy superSmartParkingBoy = new ParkingBoy(List.of(parkingLot1, parkingLot2),new SuperSmartParkingBehavior());
        ParkingTicket usedParkingTicket = superSmartParkingBoy.park(new Car());
        superSmartParkingBoy.fetch(usedParkingTicket);
        //when
        //then
        UnrecognizedParkingTicketException exception = assertThrows(UnrecognizedParkingTicketException.class, () -> superSmartParkingBoy.fetch(usedParkingTicket));
        assertEquals("Unrecognized parking ticket.", exception.getMessage());
    }

    @Test
    void should_return_nothing_with_error_message_No_available_position_when_park_given_super_smart_parking_boy_who_manage_two_parking_lots_and_both_without_any_position_and_a_car() {
        //given
        ParkingLot parkingLot1 = new ParkingLot(1, 1);
        ParkingLot parkingLot2 = new ParkingLot(1, 2);
        ParkingBoy superSmartParkingBoy = new ParkingBoy(List.of(parkingLot1, parkingLot2),new SuperSmartParkingBehavior());
        superSmartParkingBoy.park(new Car());
        superSmartParkingBoy.park(new Car());
        //when
        //then
        NoAvailablePositionException exception = assertThrows(NoAvailablePositionException.class, () -> superSmartParkingBoy.park(new Car()));
        assertEquals("No available position.", exception.getMessage());
    }
}
